﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeliriumEncounterCounter
{
    /// <summary>
    /// Interaction logic for RewardCounter.xaml
    /// </summary>
    public partial class RewardCounter : UserControl
    {

        public RewardCounter(Reward reward)
        {
            InitializeComponent();
            Reward = reward;
            this.DataContext = reward;
        }
        public Reward Reward { get; set; }
        private void Reward_Button_Click(object sender, RoutedEventArgs e)
        {
            Reward.thisClassLiterallyJustStoresADouble.All=1;
            Reward.Count=1;
        }
    }
}
