﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeliriumEncounterCounter
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            thisClassLiterallyJustStoresADouble = new ThisClassLiterallyJustStoresADouble();
            Data = new DataAccess(thisClassLiterallyJustStoresADouble);
            FillGrid(Data.CounterList);
            this.DataContext = thisClassLiterallyJustStoresADouble;
            MessageBox.Show($"1.0 {Environment.NewLine}+Things are now saved and loaded {Environment.NewLine}+You can click buttons to increase the counters", "Changelog", MessageBoxButton.OK, MessageBoxImage.None);
        }
        public ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble;

        public DataAccess Data { get; set; }

        private void Reset_Button_Click(object sender, RoutedEventArgs e)
        {
            
            var result=MessageBox.Show("Are you sure?","Reset",MessageBoxButton.YesNo,MessageBoxImage.Question);
            if (result == System.Windows.MessageBoxResult.Yes)
            {
                foreach (var item in Data.CounterList)
                {
                    item.Reward.Set(0);
                    thisClassLiterallyJustStoresADouble.Set(0);
                }
            }
        }

        private void FillGrid(List<RewardCounter> rewardCounterList)
        {
            RewardCounter rewardCounter;
            
            for (int j = 0; j < 11; ++j)
            {
                rewardCounter = rewardCounterList[j];
                Grid.SetColumn(rewardCounter, 0);
                Grid.SetRow(rewardCounter, j);
                MainGrid.Children.Add(rewardCounter);
            }
            for (int j = 0; j < 11; ++j)
            {
                rewardCounter = rewardCounterList[j+11];
                Grid.SetColumn(rewardCounter,1);
                Grid.SetRow(rewardCounter, j);
                MainGrid.Children.Add(rewardCounter);
            }

        }
        protected override void OnClosing(CancelEventArgs e)
        {
            e.Cancel = true;
            base.OnClosing(e);
            Data.Save();
            Application.Current.Shutdown();
        }
    }
}
