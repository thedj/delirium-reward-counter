﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DeliriumEncounterCounter
{
    public class ThisClassLiterallyJustStoresADouble:INotifyPropertyChanged
    {
        private double all;
        public double All
        {
            get { return all; }
            set { 
                all += value;
                OnPropertyRaised();
                totalChanged.Invoke(this,new EventArgs());
            }
        }

        public event EventHandler totalChanged;
        public event PropertyChangedEventHandler PropertyChanged;
        
        public void Set(double total)
        {
            all = total;
            OnPropertyRaised();
            totalChanged.Invoke(this, new EventArgs());
        }
        private void OnPropertyRaised()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("All"));
            }
        }
    }
}
