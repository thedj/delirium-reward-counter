﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Windows;
using CsvHelper;

namespace DeliriumEncounterCounter
{
    public class DataAccess
    {

        public DataAccess(ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble)
        {
            ListFill(CounterList,thisClassLiterallyJustStoresADouble);
            if (File.Exists("save.txt"))
            {
                Load(thisClassLiterallyJustStoresADouble);
            }
        }
        public List<RewardCounter> CounterList = new List<RewardCounter>();

        public void ListFill(List<RewardCounter> rewardCounters,ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble)
        {
            rewardCounters.Add(new RewardCounter(new Reward("Abyss", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Armour", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Blight", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Breach", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Currency", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Div Cards", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Essences", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Fossils", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Fragments", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Gems", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Harbinger", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Lab", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Legion", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Maps", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Metamorph", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Perandus", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Prophecies", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Scarabs", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Talismans", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Trinkets", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Uniques", 0, thisClassLiterallyJustStoresADouble)));
            rewardCounters.Add(new RewardCounter(new Reward("Weapon", 0, thisClassLiterallyJustStoresADouble)));
        }
        public List<int> counters = new List<int>();
        
        public void Save() 
        {
            counters.Clear();
            foreach (var item in CounterList)
            {
                counters.Add(item.Reward.Count);
            }
            var streamWriter = new StreamWriter("save.txt");
            
            foreach (var item in counters)
            {
                streamWriter.WriteLine(item.ToString());
                streamWriter.Flush();
            }
            streamWriter.Close();
        }

        public void Load(ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble)
        {
            var streamReader = new StreamReader("save.txt");
            string line;
            while ((line = streamReader.ReadLine()) != null)
            {
                counters.Add(int.Parse(line));
            }

            for (int i = 0; i < CounterList.Count; i++)
            { 
                CounterList[i].Reward.Set(counters[i]);
            }

            int total = 0;
            foreach (var item in counters)
            {
                total += item;
            }
            thisClassLiterallyJustStoresADouble.Set(total);
        }
    }
}
