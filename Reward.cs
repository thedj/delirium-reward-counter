﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace DeliriumEncounterCounter
{
    public class Reward:INotifyPropertyChanged
    {
        public Reward(string name,int count,ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble)
        {
            Name = name;
            this.count = count;
            percent = 0;
            this.thisClassLiterallyJustStoresADouble = thisClassLiterallyJustStoresADouble;
            this.thisClassLiterallyJustStoresADouble.totalChanged += totalEventHandler;

        }
        public string FullText
        {
            get { return Count+"  "+Name+"  "+Percent+"%"; }
        }
       


        public ThisClassLiterallyJustStoresADouble thisClassLiterallyJustStoresADouble;
        public string Name { get; set; }
        private int count;
        public int Count {
            get {return count;}
            set {count+=value ; OnPropertyRaised(); } 
        }

        private double percent;

        public double Percent
        {
            get
            {
                if (thisClassLiterallyJustStoresADouble.All!=0)
                {
                return (int)((Count/thisClassLiterallyJustStoresADouble.All)*100);
                }
                else
                {
                    return 0;
                }
            }
            set { percent = value; }
        }

        public void totalEventHandler(object sender,EventArgs e)
        { 
            OnPropertyRaised(); 
        }
        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyRaised()
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs("FullText"));
            }
        }
        public void Set(int i)
        {
            count = i;
            OnPropertyRaised();
        }
    }
}
